import express from "express"
import bodyParser from "body-parser"
import dotenv from "dotenv"
import cors from "cors"
import httpStatus from "http-status"

import userRoutes from "./src/user/user.routes"
import clientRoutes from "./src/client/client.routes"
import securityRoutes from './src/security/security.routes'


const app = express();


dotenv.config();

const env = process.env;
if(env.NODE_ENV === 'development'){
    app.use(cors());
}

//middleware
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


app.use('/auth', userRoutes);
app.use('/client', clientRoutes);
app.use('/security', securityRoutes);


//Si on se rend ici, alors le endpoint n'est pas valide (aucun traitement ne lui est associé)
app.use((req,res) => {
    res.status(httpStatus.NOT_FOUND).send({url:req.originalUrl + 'not found'})
});

//Message au lancement du serveur
app.listen(env.PORT, () =>
    console.log(`node server running on PORT ${env.PORT}`),);
