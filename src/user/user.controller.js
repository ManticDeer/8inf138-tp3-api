import userDB from './user.data'
import httpStatus from 'http-status'
import bcrypt from 'bcrypt'
import cryptoJS from 'crypto-js';
import JSONFormatter from '../utils/TDESFormat'
import whitelist from "./userWhitelist"
import securityConfig from "../security/securityConfig"
import logger from "../utils/logger"
import {validatePassword} from '../security/passwordValidator'
import { http } from 'winston';



//Ajouter token pour la session?

async function login(req,res){                          //Utiliser crypto-js pour faire du DES   || En-tetes types (Authorization Basic, etc)

    

    let body = req.body;
    let userData;
    try{
        
        let decrypted = cryptoJS.TripleDES.decrypt(body.contenu, "maClef", {format: JSONFormatter}); //Passe JSON contenant info au formatteur/déchiffreur
        userData = JSON.parse(decrypted.toString(cryptoJS.enc.Utf8));                                //Puis récupère les infos a l'intérieur
    }
    catch(e){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }
                                           
    //Partie verif authentifiants
    try{
        //Pour tous les usagers
        for (let i = 0; i < userDB.users.length; i++) {
            //Cherche username
            if(userDB.users[i].username === userData.username){
                if (userDB.users[i].failedLogins >= securityConfig.tentaMax) {     //Si a manqué trop de tentative, compte est barré, passe pas
                    logger.info("Tentative de connection l'utilisateur " + userDB.users[i].username + ", mais son compte est désactivé");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
                //Vérifie le mot de passe
                if(bcrypt.compareSync(userData.password, userDB.users[i].password)){
                    userDB.users[i].failedLogins = 0;                                                               //Reset tentatives échouées
                    let leToken = await whitelist.addToWhitelist(userDB.users[i].username, userDB.users[i].role);   //CHIFFRÉ AU RETOUR
                    logger.info("Utilisateur " + userDB.users[i].username + " s'est authentifié");
                    return res.status(httpStatus.OK).send({token: leToken, username: userDB.users[i].username, role: userDB.users[i].role});             //Login réussis                 
                }
                else{
                    userDB.users[i].failedLogins += 1;
                    logger.info("Tentative de connection échouée (échec #" + userDB.users[i].failedLogins + " pour l'utilisateur " + userDB.users[i].username + ")");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
            }   
        }
    }
    catch(e){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).send("Internal server Error");
    }
    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");                   //Pas réussi
}


async function logout(req,res){
    //recupere le contenu authorization des headers et verifie
    let authHeader = req.headers["authorization"] || req.headers["Authorization"];
    if(!authHeader){
        res.status(403).send("Unauthorized");
        return;
    }

    let authType = authHeader.split(" ")[0];
    let authToken = authHeader.split(" ")[1];

    if (authType != "Bearer"){
        return res.status(403).send("Unauthorized");
    }

    try{
        await whitelist.removeFromWhitelist(authToken);       //Retire de la whitelist (qu'il y soit présent ou non)
    }
    catch(e){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).send("Internal server error");
    }
    return res.status(httpStatus.OK).send("Success");
}


async function changePassword(req,res){

    let body = req.body;
    let userData;
    let decoded;

    //recupere le contenu authorization des headers et verifie
    let authHeader = req.headers["authorization"] || req.headers["Authorization"];
    if(!authHeader){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    let authType = authHeader.split(" ")[0];
    let authToken = authHeader.split(" ")[1];

    //Verifie le token
    if (authType != "Bearer"){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        decoded = await whitelist.checkWhitelist(authToken);
        
    }
    catch(e){
        return res.status(440).send("Unauthorized");
    }

    //Dechiffre les infos du 2e login
    try{
        let decrypted = cryptoJS.TripleDES.decrypt(body.contenu, "maClef", {format: JSONFormatter}); //Passe JSON contenant info au formatteur/déchiffreur
        userData = JSON.parse(decrypted.toString(cryptoJS.enc.Utf8));                                //Puis récupère les infos a l'intérieur
    }
    catch(e){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    //Verifie le 2e login
    if(userData.username != decoded.username){      //Verif token et info entrée match
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        for (let i = 0; i < userDB.users.length; i++) {
            //Cherche username
            if(userDB.users[i].username === userData.username){
                if (userDB.users[i].failedLogins >= securityConfig.tentaMax) {     //Si a manqué trop de tentative, compte est barré, passe pas
                    logger.info("Tentative de changement de mot de passe de l'utilisateur " + userDB.users[i].username + ", mais son compte est désactivé");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
                //Vérifie le mot de passe
                if(bcrypt.compareSync(userData.oldPassword, userDB.users[i].password)){
                    userDB.users[i].failedLogins = 0;                                                               //Reset tentatives échouées
                    if(validatePassword(userData.newPassword)){
                        let salt = bcrypt.genSaltSync(userDB.iterations);
                        userDB.users[i].password = bcrypt.hashSync(userData.newPassword, salt);
                        logger.info("Changement de mot de passe réussi pour l'utilisateur "+userDB.users[i].username);
                        return res.status(httpStatus.OK).send("Ok"); 
                    }
                    else{
                        return res.status(httpStatus.BAD_REQUEST).send("Respect Password policy");
                    }
                                   
                }
                else{
                    userDB.users[i].failedLogins += 1;
                    logger.info("Tentative de changement de mot de passe échoué (échec #" + userDB.users[i].failedLogins + " pour l'utilisateur " + userDB.users[i].username + ")");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
            }   
        }
    }
    catch(e){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).send("Internal server Error");
    }

    //Envoie ok pour éviter donner infos, mais rien de fait
    return res.status(httpStatus.OK).send("OK");
}

export {login, logout, changePassword}