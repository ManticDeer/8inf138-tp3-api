import jwt from 'jsonwebtoken'
import httpStatus from 'http-status'

const jsonKey = "MarioAimeLaFondueParmesanBienEpicee";
const lifetime = 120; //En secondes

let whitelist = [];

async function addToWhitelist(username, role){

    let leToken = jwt.sign({username: username, role: role}, jsonKey, {expiresIn: lifetime});

    whitelist.push({token: leToken, date: Date.now()});
    cleanWhitelist();   //Nettoyage
    return leToken;

}

async function checkWhitelist(toCheck){
    //Verifie si le token est encore valide (pas expiré), et si il est dans la whitelist
    try{
        let decoded = jwt.verify(toCheck, jsonKey);
        for (let i = 0; i < whitelist.length; i++) {
            if (whitelist[i].token == toCheck) {
                return decoded;
            }
            
        }
    }
    catch(e){}
    cleanWhitelist();   //Appel ponctuel de nettoyage
    throw new Error();
}

async function removeFromWhitelist(toRemove){
    cleanWhitelist();
    for (let i = 0; i < whitelist.length; i++) {
        if (whitelist[i].token == toRemove) {
            whitelist.splice(i, 1);
            return
        }
        
    }
}

//Vide les tokens expirés de la whitelist
async function cleanWhitelist(){
    whitelist = whitelist.filter( pasExpiré => (Date.now() - pasExpiré.date)/1000 < lifetime);
}

export default {addToWhitelist, checkWhitelist, removeFromWhitelist};