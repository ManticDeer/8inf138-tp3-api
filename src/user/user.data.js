import bcrypt from 'bcrypt'

class usersDB{
    
    //gESTION DES TENTATIVES
    constructor(){
        this.iterations = 10;
        this.users = [];
        let usernames = ["Utilisateur2", "Utilisateur1", "Administrateur"];
        let passwords = ["8INF139$", "Securite$", "DesReseaux$"];

        for (let i = 0; i < usernames.length; i++) {
            let salt = bcrypt.genSaltSync(this.iterations);
            this.users[i] = {
                username : usernames[i],
                password : bcrypt.hashSync(passwords[i], salt),
                role: i,
                algo: "bcrypt",
                failedLogins : 0
            };
            //console.log(this.users[i]);
        }

    }
}







export default new usersDB;