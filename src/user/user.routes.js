import express from 'express'
import {login, signUp, logout, changePassword} from './user.controller'

const router = express.Router();

router.route('/login').post(login);
router.route('/logout').post(logout);
router.route('/changePswd').post(changePassword);


export default router;