import passwordValidator from 'password-validator'

let schema = new passwordValidator();

schema
    .is().min(8)
    .has().uppercase()
    .has().lowercase()
    .has().digits()
    .has().symbols()
;

function validatePassword(password){
    return schema.validate(password);   //true ou false
}

function changePolicy(minLength, upperCase, lowerCase, digits, symbols){
    let newSchema = new passwordValidator();
    if (minLength > 0) {
        newSchema.is().min(minLength);
    }
    else{
        newSchema.is().min(1);
    }

    if (upperCase > 0) {
        newSchema.has().uppercase(upperCase);
    }

    if (lowerCase > 0) {
        newSchema.has().lowercase(lowerCase);
    }

    if (digits > 0) {
        newSchema.has().digits(digits);
    }

    if (symbols > 0) {
        newSchema.has().symbols(symbols);
    }

    schema = newSchema;
}

export {validatePassword, changePolicy};



