

const securityConfig = {
    tentaMax: 3,

    changeTentaMax : function(nouvelleValeur){
        this.tentaMax = nouvelleValeur;
    }
}

export default securityConfig;