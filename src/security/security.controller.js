import httpStatus from 'http-status'
import securityConfig from './securityConfig'
import {changePolicy} from './passwordValidator'
import bcrypt from 'bcrypt'
import cryptoJS from 'crypto-js';
import JSONFormatter from '../utils/TDESFormat'
import whitelist from "../user/userWhitelist"
import logger from "../utils/logger"
import userDB from "../user/user.data"



async function securityPolicyChange(req,res){
    let body = req.body;
    let userData;
    let decoded;

    //recupere le contenu authorization des headers et verifie
    let authHeader = req.headers["authorization"] || req.headers["Authorization"];
    if(!authHeader){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    let authType = authHeader.split(" ")[0];
    let authToken = authHeader.split(" ")[1];

    //Verifie le token
    if (authType != "Bearer"){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        decoded = await whitelist.checkWhitelist(authToken);
        
    }
    catch(e){
        return res.status(440).send("Unauthorized");
    }

    //Dechiffre les infos du 2e login
    try{
        let decrypted = cryptoJS.TripleDES.decrypt(body.contenu, "maClef", {format: JSONFormatter}); //Passe JSON contenant info au formatteur/déchiffreur
        userData = JSON.parse(decrypted.toString(cryptoJS.enc.Utf8));                                //Puis récupère les infos a l'intérieur
    }
    catch(e){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }
    //Verifie le 2e login


    if(decoded.username != userData.username || decoded.role != 2){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        for (let i = 0; i < userDB.users.length; i++) {
            //Cherche username
            if(userDB.users[i].username === userData.username){
                if (userDB.users[i].failedLogins >= securityConfig.tentaMax) {     //Si a manqué trop de tentative, compte est barré, passe pas
                    logger.info("Tentative de changement des politiques de sécurité par  " + userDB.users[i].username + ", mais son compte est désactivé");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
                //Vérifie le mot de passe
                if(bcrypt.compareSync(userData.password, userDB.users[i].password)){
                    logger.info("Modfication des politiques de sécurité par " + userData.username);
                    securityConfig.changeTentaMax(userData.tentaMax);
                    return res.status(httpStatus.OK).send("Ok");
                }
                else{
                    userDB.users[i].failedLogins += 1;
                    logger.info("Tentative de changement des politiques de sécurité par " + userDB.users[i].username + ", mais mot de passe érroné");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
            }   
        }
    }
    catch(e){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).send("Internal server Error");
    }
}

async function passwordPolicyChange(req,res){
    let body = req.body;
    let userData;
    let decoded;

    //recupere le contenu authorization des headers et verifie
    let authHeader = req.headers["authorization"] || req.headers["Authorization"];
    if(!authHeader){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    let authType = authHeader.split(" ")[0];
    let authToken = authHeader.split(" ")[1];

    //Verifie le token
    if (authType != "Bearer"){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        decoded = await whitelist.checkWhitelist(authToken);
        
    }
    catch(e){
        return res.status(440).send("Unauthorized");
    }

    //Dechiffre les infos du 2e login
    try{
        let decrypted = cryptoJS.TripleDES.decrypt(body.contenu, "maClef", {format: JSONFormatter}); //Passe JSON contenant info au formatteur/déchiffreur
        userData = JSON.parse(decrypted.toString(cryptoJS.enc.Utf8));                                //Puis récupère les infos a l'intérieur
    }
    catch(e){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }
    //Verifie le 2e login
    if(decoded.username != userData.username || decoded.role != 2){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        for (let i = 0; i < userDB.users.length; i++) {
            //Cherche username
            if(userDB.users[i].username === userData.username){
                if (userDB.users[i].failedLogins >= securityConfig.tentaMax) {     //Si a manqué trop de tentative, compte est barré, passe pas
                    logger.info("Tentative de changement des politiques de Mdp par  " + userDB.users[i].username + ", mais son compte est désactivé");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
                //Vérifie le mot de passe
                if(bcrypt.compareSync(userData.password, userDB.users[i].password)){
                    logger.info('Modification des politiques de mot de passe par ' + userData.username);
                    changePolicy(userData.minLength, userData.upperCase, userData.lowerCase, userData.digits, userData.symbols);
                    return res.status(httpStatus.OK).send("Ok");
                }
                else{
                    userDB.users[i].failedLogins += 1;
                    logger.info("Tentative de changement des politiques de Mdp par " + userDB.users[i].username + ", mais mot de passe érroné");
                    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
                }
            }   
        }
    }
    catch(e){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).send("Internal server Error");
    }
}

export {securityPolicyChange, passwordPolicyChange}