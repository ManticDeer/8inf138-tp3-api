import express from 'express'
import {securityPolicyChange, passwordPolicyChange} from './security.controller'

const router = express.Router();

router.route('/securityPolicy').post(securityPolicyChange);
router.route('/passwordPolicy').post(passwordPolicyChange);

export default router;