import express from 'express'
import {getResidentiel, getAffaire} from "./client.controller"

const router = express.Router();

router.route('/residentiel').get(getResidentiel);
router.route('/affaire').get(getAffaire);


export default router;