import httpStatus from "http-status";
import clientsResidentiel from './clientRes.data'
import clientsAffaire from './clientAff.data'
import whitelist from '../user/userWhitelist'
import logger from "../utils/logger"



async function getResidentiel(req,res){

    //recupere le contenu authorization des headers et verifie
    let authHeader = req.headers["authorization"] || req.headers["Authorization"];
    if(!authHeader){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    let authType = authHeader.split(" ")[0];
    let authToken = authHeader.split(" ")[1];

    if (authType != "Bearer"){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        let decoded = await whitelist.checkWhitelist(authToken);
        if (decoded.role == 1 || decoded.role == 2 ) {
            logger.info("Clients résidentiels demandés par " + decoded.username);
            return res.status(httpStatus.OK).send(clientsAffaire);
        }
    }
    catch(e){

    }

    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
}

async function getAffaire(req,res){

    //recupere le contenu authorization des headers et verifie
    let authHeader = req.headers["authorization"] || req.headers["Authorization"];
    if(!authHeader){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    let authType = authHeader.split(" ")[0];
    let authToken = authHeader.split(" ")[1];

    if (authType != "Bearer"){
        return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
    }

    try{
        let decoded = await whitelist.checkWhitelist(authToken);
        if (decoded.role == 0 || decoded.role == 2 ) {
            logger.info("Clients affaires demandés par " + decoded.username);
            return res.status(httpStatus.OK).send(clientsResidentiel);
        }
    }
    catch(e){

    }

    return res.status(httpStatus.UNAUTHORIZED).send("Unauthorized");
}

export {getResidentiel, getAffaire};