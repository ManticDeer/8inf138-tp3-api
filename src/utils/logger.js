import winston from 'winston'


const logger = winston.createLogger({
    level: 'info',
    transports:[
        new winston.transports.File({ filename: './src/logs/log.log' })
    ],
    format: winston.format.combine(
        winston.format.label({label: "API"}),
        winston.format.timestamp(),
        winston.format.printf(({ level, message, label, timestamp }) => {
            return `[${label}]: ${timestamp} ${level}: ${message}`;
          })

    )

});

export default logger;