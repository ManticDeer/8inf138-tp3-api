import cryptoJS from 'crypto-js'


//TIRÉ/COPIÉ DE LA DOCUMENTATION DE https://cryptojs.gitbook.io/docs/#ciphers 
const JSONFormatter = {
    stringify: function(cipherParams) {
        // create json object with ciphertext
        var jsonObj = { ct: cipherParams.ciphertext.toString(cryptoJS.enc.Base64) };
        // optionally add iv or salt
        if (cipherParams.iv) {
          jsonObj.iv = cipherParams.iv.toString();
        }
        if (cipherParams.salt) {
          jsonObj.s = cipherParams.salt.toString();
        }
        // stringify json object
        return JSON.stringify(jsonObj);
      },

    parse: function(jsonStr) {
        // parse json string
        var jsonObj = JSON.parse(jsonStr);
        // extract ciphertext from json object, and create cipher params object
        var cipherParams = cryptoJS.lib.CipherParams.create({
          ciphertext: cryptoJS.enc.Base64.parse(jsonObj.ct)
        });
        // optionally extract iv or salt
        if (jsonObj.iv) {
          cipherParams.iv = cryptoJS.enc.Hex.parse(jsonObj.iv);
        }
        if (jsonObj.s) {
          cipherParams.salt = cryptoJS.enc.Hex.parse(jsonObj.s);
        }
        return cipherParams;
    }
};

export default JSONFormatter;

//User: Chiffre un JSON contenant auth avec le cipher, recupere iv salt ciphertext et met dans un objet. Met en JSON pour requete
//Server: Recoit data, si sous forme JSON, passe dans dechiffreur avec formatteur. (Si pas en JSON, doit le rendre avec JSON.STRINGIFY, PAS LE FORMATTEUR, CAR C'EST PAS UN CIPHERPARAMS)
//Une fois déchiffré, on passe en encodage UTF-8, et on parse le JSON. On a les données


//Notes de pratiques
// let aCrypter = {
//     username: "Administrateur",
//     password: "DesReseaux$"
// };
// let encrypted = cryptoJs.TripleDES.encrypt(JSON.stringify(aCrypter), "maClef");
// console.log(JSONFormatter.stringify(encrypted));

// let decrypted = cryptoJs.TripleDES.decrypt(JSONFormatter.stringify(encrypted), "maClef", {format: JSONFormatter});
// console.log(JSON.parse(decrypted.toString(cryptoJs.enc.Utf8)));